﻿using Newtonsoft.Json;
using System;

namespace App1
{
    public class NewsManager
    {
        private string dt = "1491830525";
        private string nCount = "50";
        private string webResource = "http://informer.maximarkets.ru/api/feed/ru/";

        public NewsManager()
        {
            webResource += dt + '/' + nCount;
        }

        public string GetWebResourse()
        {
            return webResource;
        }

        public void NewsParser(string data)
        {
            var res = JsonConvert.DeserializeObject<string>(data);
            DAL.newsArray = JsonConvert.DeserializeObject<News[]>(res);
            DateConvert();
        }

        public void DateConvert()
        {
            int res;
            DateTime dt;
            for (int i = 0; i < DAL.newsArray.Length; i++)
            {
                res = int.Parse(DAL.newsArray[i].PublishDate);
                dt = UnixTimeStampToDateTime(res);
                DAL.newsArray[i].PublishDate = dt.ToString();
            }
        }

        public static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
        {
            DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
            return dtDateTime;
        }

    }
}



