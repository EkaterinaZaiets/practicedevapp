﻿using System;
using System.IO;
using System.Net;

namespace App1.iOS
{
	public class WebAPI
	{

		private WebRequest webRequest;
		private HttpWebResponse webResponse;
		private NewsManager newsManager;

		public void Connection()
		{
			newsManager = new NewsManager();

			webRequest = WebRequest.Create(newsManager.GetWebResourse());
			webResponse = webRequest.GetResponse() as HttpWebResponse;

			Stream dataStream = webResponse.GetResponseStream();
			StreamReader sr = new StreamReader(dataStream);
			string response = sr.ReadToEnd();

			newsManager.NewsParser(response);

			sr.Close();
			dataStream.Close();
			webResponse.Close();
		}
	}
}


