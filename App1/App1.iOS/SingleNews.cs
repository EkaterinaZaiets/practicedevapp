using Foundation;
using System;
using UIKit;
using System.ComponentModel;

namespace App1.iOS
{
	public partial class SingleNews : UIView, IComponent

	{
		public static readonly NSString Key = new NSString("SingleNews");
		public static readonly UINib Nib;



		static SingleNews()
		{
			Nib = UINib.FromName("SingleNews", NSBundle.MainBundle);
		}

		public SingleNews (IntPtr handle) : base (handle)
        {
        }

		public ISite Site { get; set; }


		public event EventHandler Disposed;

		public override void AwakeFromNib()
		{
			if ((Site != null) && Site.DesignMode)
			{
				return;
			}

			NSBundle.MainBundle.LoadNib("SingleNews", this, null);

			var frame = singleNews.Frame;
			frame.Height = Frame.Height;
			frame.Width = Frame.Width;
			singleNews.Frame = frame;
			this.AddSubview(this.singleNews);
		}

		public void SetData(string title, string data,string description, string img)
		{
			titleLabel.Text = title;
			dateLabel.Text = data;
			imNews.Image = NewsCard.GetUIImageFromBase64(img);
			descriptionLabel.Text = description;
		}

	}
}