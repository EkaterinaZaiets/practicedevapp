// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace App1.iOS
{
    [Register ("ViewControllerSnglNews")]
    partial class ViewControllerSnglNews
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIScrollView scrollView1 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        App1.iOS.SingleNews singleNews { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (scrollView1 != null) {
                scrollView1.Dispose ();
                scrollView1 = null;
            }

            if (singleNews != null) {
                singleNews.Dispose ();
                singleNews = null;
            }
        }
    }
}