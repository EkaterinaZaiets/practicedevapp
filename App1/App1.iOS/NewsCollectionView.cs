using Foundation;
using System;
using UIKit;

namespace App1.iOS
{
    public partial class NewsCollectionView : UICollectionView
    {
       

		public static AppDelegate App
		{
			get { return (AppDelegate)UIApplication.SharedApplication.Delegate; }
		}

		public CustomCollectionSourse Sourse
		{
			get { return DataSource as CustomCollectionSourse;}
		}


 		public NewsCollectionView (IntPtr handle) : base(handle)
		{
			
		}

		public void CustomInit()
		{
			RegisterNibForCell(UINib.FromName(NewsCard.Key, NSBundle.MainBundle), NewsCard.Key);
		}
    }
}