﻿using System;
using CoreGraphics;
using Foundation;
using UIKit;
namespace App1.iOS
{
	public class NewsViewDelegate : UICollectionViewDelegateFlowLayout
	{

		private Action<int> action;

		public static AppDelegate App
		{
			get
			{
				return (AppDelegate)UIApplication.SharedApplication.Delegate;
			}
		}

		public NewsViewDelegate(Action<int> action)
		{
			this.action = action;

		}

		[Export("collectionView:layout:sizeForItemAtIndexPath:")]
		public CoreGraphics.CGSize GetSizeForItem(UICollectionView collectionView, UICollectionViewLayout layout, NSIndexPath indexPath)
		{
			return new CGSize((float)UIScreen.MainScreen.Bounds.Size.Width, 200);
		}

		public override void ItemSelected(UICollectionView collectionView, NSIndexPath indexPath)
		{
			int selectedNewsId = indexPath.Row;

			//invoke
			action.Invoke(selectedNewsId);



		}
	}
}
