﻿using System;
using Foundation;
using UIKit;
using CoreGraphics;
using System.Collections.Generic;

namespace App1.iOS
{
	public class CustomCollectionSourse : UICollectionViewDataSource
	{

		public NewsCollectionView ViewController { get; set; }
		public static AppDelegate App
		{
			get
			{
				return (AppDelegate)UIApplication.SharedApplication.Delegate;
			}
		}

		public CustomCollectionSourse()
		{
			
		}


		public static NSString newsCellId = new NSString("NewsCell");

		public override UICollectionViewCell GetCell(UICollectionView collectionView, NSIndexPath indexPath)
		{
			var newsCell = (NewsCard)collectionView.DequeueReusableCell(NewsCard.Key, indexPath);
			var news = DAL.newsArray[indexPath.Row];


			newsCell.SetData(news.Title, news.PublishDate, news.ImageBase64);
	

			return newsCell;
		}

		public override nint GetItemsCount(UICollectionView collectionView, nint section)
		{
			return DAL.newsArray.Length;
		}





	}
}
