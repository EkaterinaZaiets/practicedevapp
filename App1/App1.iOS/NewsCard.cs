﻿using System;

using Foundation;
using UIKit;

namespace App1.iOS
{
	public partial class NewsCard : UICollectionViewCell 
	{
		public static readonly NSString Key = new NSString("NewsCard");
		public static readonly UINib Nib;



		static NewsCard()
		{
			Nib = UINib.FromName("NewsCard", NSBundle.MainBundle);

		}

		public NewsCard(IntPtr handle) : base(handle)
		{
		}

		public void SetData(string title, string data, string img)
		{
			titleLabel.Text = title;
			dateLabel.Text = data;
			imNews.Image = GetUIImageFromBase64(img);
		}

		public override void AwakeFromNib()
		{
			base.AwakeFromNib();
		}

		public static UIImage GetUIImageFromBase64(string base64Image)
		{
			byte[] encodedDataAsBytes = System.Convert.FromBase64String(base64Image);
			NSData data = NSData.FromArray(encodedDataAsBytes);
			return UIImage.LoadFromData(data);
		}


	}
}
