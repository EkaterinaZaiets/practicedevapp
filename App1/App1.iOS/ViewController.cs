﻿using System;
using Foundation;
using UIKit;

namespace App1.iOS
{
	public partial class ViewController : UIViewController 
	{
		public NewsCollectionView cv;

		public ViewController(IntPtr handle) : base(handle)
		{
		}

		//public static AppDelegate App
		//{
		//	get
		//	{
		//		return (AppDelegate)UIApplication.SharedApplication.Delegate;
		//	}
		//}






		//creste action! call prep f seg

		private void GoToSingleView(int pos)
		{
			var controller = UIStoryboard.FromName("Main", null).InstantiateViewController("SingleNews") as ViewControllerSnglNews;
			controller.FillData(pos);
			NavigationController.PushViewController( controller, true);

		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			collectionView.CustomInit();

			var dataSource = new CustomCollectionSourse();
			collectionView.DataSource = dataSource;
			var delegateq = new NewsViewDelegate(GoToSingleView);
			collectionView.Delegate = delegateq;

		}

		public override void DidReceiveMemoryWarning()
		{
			base.DidReceiveMemoryWarning();
		}

		public nint GetItemsCount(UICollectionView collectionView, nint section)
		{
			throw new NotImplementedException();
		}

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);


		}

		public override void PrepareForSegue(UIStoryboardSegue segue, NSObject sender)
		{
			base.PrepareForSegue(segue, sender);

		}

		public override void LoadView()
		{
			base.LoadView();
		//	View = collectionView;
		}
	}
}



