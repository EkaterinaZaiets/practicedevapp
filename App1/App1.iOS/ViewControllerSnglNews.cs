using Foundation;
using System;
using UIKit;
using System.ComponentModel;
using CoreGraphics;

namespace App1.iOS
{
    public partial class ViewControllerSnglNews : UIViewController
    {
        public ViewControllerSnglNews (IntPtr handle) : base (handle){}

		private int _id;
		//UIScrollView scrollView;

		public void FillData(int id)
		{
			_id = id;
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			//scrollView = new UIScrollView(new CGRect(0, 0, View.Frame.Width, View.Frame.Height));
			//View.AddSubview(scrollView);



			singleNews.SetData(DAL.newsArray[_id].Title, DAL.newsArray[_id].PublishDate,
							   DAL.newsArray[_id].Description, DAL.newsArray[_id].ImageBase64);

			//scrollView1.ContentSize = singleNews.Frame.Size;
			//scrollView1.AddSubview(singleNews);
		}

	}
}