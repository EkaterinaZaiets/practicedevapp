﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App1
{
    public class News
    {
        public string ID { get; set; }
        public string Title { get; set; }
        public string PublishDate { get; set; }
        public string ImageBase64 { get; set; }

        public News(string ID, string Title, string PublishDate, string ImageBase64)
        {
            this.ID = ID;
            this.Title = Title;
            this.PublishDate = PublishDate;
            this.ImageBase64 = ImageBase64;
        }
    }
}
