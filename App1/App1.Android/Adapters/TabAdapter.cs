﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SupportFragment = Android.Support.V4.App.Fragment;
using SupportFragmentManager = Android.Support.V4.App.FragmentManager;
using Android.Support.V4.App;

namespace App1.Droid
{
    public class TabAdapter : FragmentPagerAdapter
    {
        List<SupportFragment> tabList { get; set; }
        List<string> tabTiteList { get; set; }

        public TabAdapter(SupportFragmentManager mgr) : base(mgr)
        {
            tabList = new List<SupportFragment>();
            tabTiteList = new List<string>();
        }

        public void AddFragment(SupportFragment fragment, string title)
        {
            tabList.Add(fragment);
            tabTiteList.Add(title);
        }

        public override int Count
        {
            get
            {
                return tabList.Count;
            }
        }

        public override SupportFragment GetItem(int position)
        {
            return tabList[position];
        }

        public string GetTabTitle(int position)
        {
            return tabTiteList[position];
        }

        public override Java.Lang.ICharSequence GetPageTitleFormatted(int position)
        {
            return new Java.Lang.String(tabTiteList[position]);
        }
    }
}