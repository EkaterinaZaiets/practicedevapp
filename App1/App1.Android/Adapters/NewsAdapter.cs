﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.Widget;
using System.Threading;
using Android.Graphics;
using Android.Util;

namespace App1.Droid
{
    public class NewsAdapter : RecyclerView.Adapter
    {
        private NewsManager _newsManager;
		public event EventHandler<int> ItemClick;

		public void OnClick(int position)
		{
			if (ItemClick != null)
			{
				ItemClick(this, position);
			}
		}


		public NewsAdapter(NewsManager newsManager)
        {
            _newsManager = newsManager;
        }

        public override int ItemCount
        {
            get { return DAL.newsArray.Length; }
        }

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            NewsViewHolder vh = holder as NewsViewHolder;
            News[] news = DAL.newsArray;
            try
            {
                vh.Title.Text = news[position].Title;
                vh.Date.Text = news[position].PublishDate;
                vh.Image.SetImageBitmap(ImageFromBase64(news[position].ImageBase64));
            }
            catch (IndexOutOfRangeException ex)
            {
                
            }
            
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            View itemView = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.news_card, parent, false);

			NewsViewHolder vh = new NewsViewHolder(itemView, OnClick);
            return vh;
        }

        internal static Bitmap ImageFromBase64(string imgSource)
        {
            Bitmap bitmap = null;
            try
            {
                byte[] data = Base64.Decode(imgSource, Base64Flags.Default);
                bitmap = BitmapFactory.DecodeByteArray(data, 0, data.Length);
            }
            catch (Exception e)
            {
                Log.Error("ImageFromBase64   ", e.Message);
            }
            return bitmap;
        }
    }
}