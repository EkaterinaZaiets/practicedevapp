﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Support.V4.App;
using Android.Support.V4.View;
using SupportFragment = Android.Support.V4.App.Fragment;
using SupportFragmentManager = Android.Support.V4.App.FragmentManager;
using Android.Support.V7.Widget;
using System.Threading;

namespace App1.Droid
{
    [Activity (Label = "App1.Android", MainLauncher = true, Icon = "@drawable/icon")]
	public class MainActivity : FragmentActivity
	{
        ViewPager viewPager;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView(Resource.Layout.Main);

            WebAPI.WebAPI webApi = new WebAPI.WebAPI();
            webApi.Connection();

            viewPager = FindViewById<ViewPager>(Resource.Id.viewpager);

            TabAdapter tabAdapter = new TabAdapter(SupportFragmentManager);
            tabAdapter.AddFragment(new NewsFragment(), Resources.GetString(Resource.String.tab1));
            tabAdapter.AddFragment(new Tab2Fragment(), Resources.GetString(Resource.String.tab2));
            viewPager.Adapter = tabAdapter;

        }
	}
}


