﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace App1.Droid
{
	[Activity(Label = "SingleNewsActivity")]
	public class SingleNewsActivity : Activity
	{
		ImageView ivImage;
		TextView tvTitle;
		TextView tvDate;
		TextView tvDescription;

		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			SetContentView(Resource.Layout.sngl_view_news);

			ivImage = FindViewById<ImageView>(Resource.Id.imNews);
			tvTitle = FindViewById<TextView>(Resource.Id.tvTitle);
			tvDate = FindViewById<TextView>(Resource.Id.tvDate);
			tvDescription = FindViewById<TextView>(Resource.Id.tvDescription);

			var id = Intent.Extras.GetInt("id");

			News news = DAL.newsArray[id];

			ivImage.SetImageBitmap(NewsAdapter.ImageFromBase64(news.ImageBase64));

			tvTitle.Text = news.Title;
			tvDate.Text = news.PublishDate;
			tvDescription.Text = news.Description;

		}
	}
}
