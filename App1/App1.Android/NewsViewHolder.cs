﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.Widget;

namespace App1.Droid
{
    public class NewsViewHolder : RecyclerView.ViewHolder
    {
        public TextView     Title { get; private set; }
        public TextView     Date  { get; private set; }
        public ImageView    Image { get; private set; }

		public NewsViewHolder(View itemView, Action<int> listener) : base(itemView)
        {
            Title   = itemView.FindViewById<TextView> (Resource.Id.tvTitle);
            Date    = itemView.FindViewById<TextView> (Resource.Id.tvDate);
            Image   = itemView.FindViewById<ImageView>(Resource.Id.imNews);

			itemView.Click += (sender, e) => listener(Position);
        }

    }
}