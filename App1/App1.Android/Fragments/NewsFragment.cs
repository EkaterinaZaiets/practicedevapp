﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Fragment = Android.Support.V4.App.Fragment;
using Android.Support.V7.Widget;

namespace App1.Droid
{
    public class NewsFragment : Fragment
    {
        public NewsFragment(){ }

        private RecyclerView recyclerView;
        private NewsAdapter newsAdapter;
        private NewsManager newsManager;
        private RecyclerView.LayoutManager layoutManager;
        
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View view = inflater.Inflate(Resource.Layout.tab, container, false);
            recyclerView = (RecyclerView)view.FindViewById(Resource.Id.recyclerView);
            newsManager = new NewsManager();
            newsAdapter = new NewsAdapter(newsManager);

            newsManager = new NewsManager();

            layoutManager = new LinearLayoutManager(this.Context);
            recyclerView.SetLayoutManager(layoutManager);

            newsAdapter = new NewsAdapter(newsManager);
			newsAdapter.ItemClick += OnItemClick;

            recyclerView.SetAdapter(newsAdapter);

            return view;
        }

		private void OnItemClick(object sender, int id)
		{
			try
			{
				Intent intent = new Intent(Context, typeof(SingleNewsActivity));
				intent.PutExtra("id", id);
				StartActivity(intent);
			}
			catch (Exception ex)
			{

			}
		}
}
}